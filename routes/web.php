<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ApplicationsController;
use App\Http\Controllers\ServicePriceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', [LoginController::class, 'login'])->name('login');

Route::get('/', function () {
    return redirect('certify-user');
    // return view('welcome');
});

Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', "LoginController@login")->name('login');
        Route::get('logout', "LoginController@logout")->name('logout');
        Route::post('authenticate', "LoginController@authenticate")->name('authenticate');
    });
});

Route::get('certify-user', function () {
    $servicePrices = \App\Models\ServicePrice::where('is_active', 1)->get();
    return view('apply')->with(['service_prices' => $servicePrices]);
});

Route::post('certify-user', [ApplicationsController::class, 'store']);
Route::resource('applications', ApplicationsController::class);
Route::post('approve-user', [ApplicationsController::class, 'approve'])->name('approve');
Route::post('accept-user', [ApplicationsController::class, 'accept'])->name('accept');
Route::post('reject-user', [ApplicationsController::class, 'reject'])->name('reject');

Route::resource('prices', ServicePriceController::class);
