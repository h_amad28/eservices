<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('users')->truncate();

        $users = [
            ['name' => 'Admin',          'type' => 'admin',         'email' => 'admin@gmail.com',      'password' => \Hash::make('admin123'), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Price Manager',  'type' => 'price_manager', 'email' => 'pm@gmail.com',         'password' => \Hash::make('pm12345'), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Manager',        'type' => 'manager',       'email' => 'manager@gmail.com',    'password' => \Hash::make('12345678'), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Employee',       'type' => 'employee',      'email' => 'employee@gmail.com',   'password' => \Hash::make('12345678'), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];

        User::insert($users);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}