## eServices
A small dynamic application.
Registering applicants. Providing actions of accepting, rejecting and approving to employee/Manager.
## Setup
1. clone the repository
2. run migration - php artisan db:seed --class=CreateUserSeeder
3. run seeder - php artisan db:seed
4. employee
-  employee@gmail.com (12345678)
5. Manager
-  manager@gmail.com (12345678)
6. admin@gmail.com
- admin123