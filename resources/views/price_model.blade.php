<div class="modal fade" id="priceModal" tabindex="-1" aria-labelledby="price" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Certification Charges</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Price</th>
                    <th>type</th>
                    <th>Vat</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($service_prices as $priceInfo)
                <tr>
                    <td><input type="radio" class="priceSelection form-check-input" name="invoice_price" value="{{ $priceInfo->id }}" /></td>
                    <td>{{ $priceInfo->price }}</td>
                    <td>{{ ucfirst($priceInfo->type) }}</td>
                    <td>{{ $priceInfo->vat }}</td>
                    <td>{{ $priceInfo->total }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary" onclick="document.getElementById('applicationForm').submit();return false;">Confirm</button> -->
        <button type="button" class="btn btn-primary disabled submitApplication" onclick="validateAppicationForm();return false;">Confirm</button>
      </div>
    </div>
  </div>
</div>