@extends('base', ['title' => 'Applications'])

@section('content')
<div class="row">
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>email</th>
            <th>Country</th>
            <th>phone</th>
            <th>Service</th>
            <th>Picture</th>
            <th>Graduation detail</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if(count($applications))
            @foreach ($applications as $info)
            <tr>
                <td>{{ $info->full_name }}</td>
                <td>{{ $info->email }}</td>
                <td>{{ ucfirst($info->country) }}</td>
                <td>{{ $info->phone }}</td>
                <td>{{ $info->serviceInvoice->title ?? '' }}({{ $info->serviceInvoice->type ?? '' }})</td>
                <td><img src="{{ Storage::url($info->applicationImages->images) }}" width="50" /></td>
                <td>
                    <ul>
                    @if($info->applicationGraduations)
                        @foreach($info->applicationGraduations as $ginfo)
                            <li><b>University:</b>&ensp;{{ $ginfo->university }}</li>
                            <li><b>Certificate:</b>&ensp;<a href="{{ Storage::url($ginfo->certificate) }}" target="_blank">{{ $ginfo->education }}</a></li>
                        @endforeach
                    @endif
                    </ul>
                </td>
                <td>
                    @if($info->status == 'approved')
                        <i class="fa-solid fa-thumbs-up"></i>
                    @elseif($info->status == 'initial acceptance')
                        <a href="#" data-id="{{ $info->id }}" class="approve-applicant" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Approve Application">
                            <i class="fa-solid fa-certificate"></i>
                        </a>
                    @else
                        <a href="#" data-id="{{ $info->id }}" class="accept-applicant"  data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Accept Application">
                            <i class="fa-solid fa-square-check"></i>
                        </a>&ensp;
                        <a href="#" data-id="{{ $info->id }}" class="reject-applicant"  data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Reject Application">
                            <i class="fa-solid fa-square-minus"></i>
                        </a>
                    @endif
                </td>
            </tr>
            @endforeach
        @else
            <tr>
                <td colspan="8">No record found</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
@endsection

@push('js')
<script>
(() => {
  'use strict'
  const forms = document.querySelectorAll('.needs-validation')
  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }
      form.classList.add('was-validated')
    }, false)
  })
})()

$(document).ready(function() {
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));
    $(".reject-applicant").click(function(e) {
        e.preventDefault();
        var th_is = $(this);
        var id = th_is.attr('data-id');
        $.ajax({
            type: "POST",
            url: "{{ route('reject') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id
            },
            success: function(data){
                th_is.parent('td').parent('tr').remove();
            }
        });
    });
    $(".accept-applicant").click(function(e) {
        e.preventDefault();
        var th_is = $(this);
        var id = th_is.attr('data-id');
        $.ajax({
            type: "POST",
            url: "{{ route('accept') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id
            },
            success: function(data){
                th_is.addClass('d-none');
                $('.fa-square-minus').addClass('d-none');
            }
        });
    });
    $(".approve-applicant").click(function(e) {
        e.preventDefault();
        var th_is = $(this);
        var id = th_is.attr('data-id');
        $.ajax({
            type: "POST",
            url: "{{ route('approve') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                id
            },
            success: function(data){
                th_is.removeAttr('href').html('<i class="fa-solid fa-thumbs-up"></i>');
            }
        });
    })
})
</script>
@endpush