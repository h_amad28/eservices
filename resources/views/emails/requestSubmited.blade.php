<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Request Submitted</title>
   
  </head>
  <body class="">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center"><h3>Application Submitted</h3></td>
        </tr>
        <tr>
            <td align="left"><b>Dear {{ ucfirst($application->full_name) }},</b></td>
        </tr>
        <tr>
            <td align="left">The request has been received and now it is under processing by company.</td>
        </tr>
        <tr>
            <td>
                Thank you - eServices
            </td>
        </tr>
    </table>
  </body>
</html>