@extends('base', ['title' => 'Service Prices'])

@section('content')
<div class="row">
    <div class="col">
        <button type="button" class="float-end btn btn-primary" data-bs-toggle="modal" data-bs-target="#addPriceModal">
            Add
        </button>
    </div>
</div>
<div class="row">
<table class="table table-striped">
    <thead>
        <tr>
            <th>Title</th>
            <th>Price</th>
            <th>Vat</th>
            <th>Vat Type</th>
            <th>Type</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @if(count($data))
            @foreach ($data as $priceInfo)
            <tr>
                <td>{{ $priceInfo->title }}</td>
                <td>{{ $priceInfo->price }}</td>
                <td>{{ $priceInfo->vat }}</td>
                <td>{{ $priceInfo->vat_type }}</td>
                <td>{{ ucfirst($priceInfo->type) }}</td>
                <td>{{ $priceInfo->total }}</td>
            </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">No record found</td>
            </tr>
        @endif
    </tbody>
</table>
</div>

<div class="modal modal-lg fade" id="addPriceModal" tabindex="-1" aria-labelledby="addPrice" aria-hidden="true">
  <div class="modal-dialog">
    <form action="" method="post" class="needs-validation" id="priceForm" enctype="multipart/form-data" novalidate>
        @csrf
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Add Price</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="row mb-1">
                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <label for="firstName" class="form-label">Title</label>
                    <input type="text" name="title" class="form-control" id="title" required />
                    <div class="invalid-feedback">Title is required.</div>
                </div>
                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <label for="lastName" class="form-label">Price</label>
                    <input type="text" class="form-control" name="price" id="price" required />
                    <div class="invalid-feedback">Price is required.</div>
                </div>
            </div>
            <div class="row mb-1">
                <div class="col col-lg-4 col-md-4 col-sm-12">
                    <label for="vat" class="form-label">Vat</label>
                    <input type="text" name="vat" class="form-control" id="vat" required />
                    <div class="invalid-feedback">Vat. is required.</div>
                </div>
                <div class="col col-lg-4 col-md-4 col-sm-12">
                    <label for="vat_type" class="form-label">Vat. Type</label>
                    <select id="vat_type" name="vat_type" class="form-select" required>
                        <option selected value="" disabled>Choose...</option>
                        <option value="percentage">Percentage</option>
                        <option value="fixed">Fixed</option>
                    </select>
                    <div class="invalid-feedback">Please select Vat. type</div>
                </div>
                <div class="col col-lg-4 col-md-4 col-sm-12">
                    <label for="type" class="form-label">Type</label>
                    <select id="type" name="type" class="form-select" required>
                        <option selected value="" disabled>Choose...</option>
                        <option value="free">Free</option>
                        <option value="paid">Paid</option>
                        <option value="certified">Certified</option>
                    </select>
                    <div class="invalid-feedback">Please select Vat. type</div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        </div>
    </form>
  </div>
</div>
@endsection

@push('js')
<script>
(() => {
  'use strict'
  const forms = document.querySelectorAll('.needs-validation')
  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }
      form.classList.add('was-validated')
    }, false)
  })
})()
</script>
@endpush