@extends('base', ['title' => 'Apply Certification'])

@section('content')
<link rel="stylesheet" href="{{ asset('css/intlTelInput.min.css') }}" />
@include('price_model')
<h1>Apply Certification</h1>
<p>Fill up the following details and submit</p>
<hr class="my-2" />
<form action="" method="post" class="needs-validation" id="applicationForm" enctype="multipart/form-data" novalidate>
    @csrf
    <h3>Personal Information</h3>
    <div class="row mb-1">
        <div class="col col-lg-4 col-sm-12">
            <input type="hidden" name="service_price" value="" />
            <input type="hidden" name="country" id="country" value="" />
            <label for="firstName" class="form-label">First Name</label>
            <input type="text" name="first_name" class="form-control" id="firstName" required />
            <div class="invalid-feedback">First name is required.</div>
        </div>
        <div class="col col-lg-4 col-sm-12">
            <label for="lastName" class="form-label">Last Name</label>
            <input type="text" class="form-control" name="last_name" id="lastName" required />
            <div class="invalid-feedback">Last name is required.</div>
        </div>
        <div class="col col-lg-4 col-sm-12">
            <label for="formFile" class="form-label">Picture</label>
            <input class="form-control btn btn-default" name="profilePicture" type="file" id="formFile" accept=".jpg,.jpeg,.png" required />
            <div class="invalid-feedback">Profile picture is required.</div>
        </div>
    </div>
    <div class="row mb-1">
        <div class="col col-lg-6 col-sm-12">
            <label for="inputEmail" class="form-label">Email</label>
            <!-- <input type="email" class="form-control" id="inputEmail" required /> -->
            <div class="input-group">
                <div class="input-group-text">@</div>
                <input type="email" class="form-control" name="email" id="inputEmail" required />
                <div class="invalid-feedback">Email is required.</div>
            </div>
        </div>
        <div class="col col-lg-6 col-sm-12">
            <label for="phone" class="form-label d-block">Phone</label>
            <input type="tel" class="form-control" name="phone" id="phone" required />
            <div class="invalid-feedback">Phone is required</div>
        </div>
    </div>
    <hr class="my-3" />
    <h2>Education</h2>
    <div class="row mb-2 cloneThis">
        <div class="col col-lg-4 col-md-4">
            <label for="university" class="form-label">University</label>
            <select id="university" name="university[]" class="form-select" required>
                <option selected value="" disabled>Choose...</option>
                <option value="ABC uni">ABC</option>
                <option value="DEF uni">DEF</option>
            </select>
            <div class="invalid-feedback">Please select university</div>
        </div>
        <div class="col col-lg-3 col-md-3">
            <label for="title" class="form-label">Title</label>
            <input type="text" name="degree[]" class="form-control" id="title" required />
            <div class="invalid-feedback">Add degree title.</div>
        </div>
        <div class="col col-lg-3 col-md-3 col-sm-12">
            <label for="formCertificateFile" class="form-label">Certificate</label>
            <input class="form-control btn btn-default" name="certificate[]" type="file" id="formCertificateFile" value="" accept=".pdf" required />
            <div class="invalid-feedback">Certificate is required.</div>
        </div>
        <div class="col col-lg-2 col-md-2 col-sm-12 d-flex align-items-center">
            <a class="fs-3 addMoreCertificate">
                <i class="fa-solid fa-square-plus"></i>
            </a>
        </div>
    </div>
    <div class="clonnedItems"></div>

    <div class="row">
        <div class="col-12">
            <!-- <button type="submit" class="btn btn-primary">Apply</button> -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#priceModal">
                Apply
            </button>
        </div>
    </div>
</form>
@endsection

@push('js')
<script src="{{ asset('js/intlTelInput.js') }}"></script>
<script src="{{ asset('js/utils.js') }}"></script>
<script>


var input = document.querySelector("#phone");
var ins = window.intlTelInput(input, {
    // allowDropdown: false,
    // autoInsertDialCode: true,
    // autoPlaceholder: "off",
    // countrySearch: true,
    // customContainer: "test",
    // customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
    //   return "e.g. " + selectedCountryPlaceholder;
    // },
    // dropdownContainer: document.querySelector('#custom-container'),
    // excludeCountries: ["us"],
    // fixDropdownWidth: true,
    // formatOnDisplay: false,
    // geoIpLookup: function(callback) {
    //   fetch("https://ipapi.co/json")
    //     .then(function(res) { return res.json(); })
    //     .then(function(data) { callback(data.country_code); })
    //     .catch(function() { callback("us"); });
    // },
    // hiddenInput: "full_number",
    // initialCountry: "auto",
    // localizedCountries: { 'de': 'Deutschland' },
    // nationalMode: false,
    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    placeholderNumberType: "MOBILE",
    // preferredCountries: ['cn', 'jp'],
    // separateDialCode: true,
    // showFlags: false,
    // useFullscreenPopup: true,
    isValidNumber: (e) => {
        console.log('e', e);
    },
    utilsScript: "build/js/utils.js"
    });
const validateAppicationForm = () => {
  'use strict'
  const forms = document.querySelectorAll('.needs-validation')
  Array.from(forms).forEach(form => {
    // form.addEventListener('submit', event => {
      var validated = true;
        if(!ins.isValidNumber()){
            $('#phone').val('');
        } else {
            $('#country').val(ins.selectedCountryData.name);
        }
      if (!form.checkValidity()) {
        event.preventDefault();
        event.stopPropagation();
        validated = false;
      }
      $('#priceModal .btn-close').click();
      form.classList.add('was-validated');
      if (validated) {
        document.getElementById('applicationForm').submit();
      }
    // }, false)
  })
}
$(document).ready(function(){
    const deleteAppIcon = '<a class="fs-3 removeCertificate"><i class="fa-solid fa-square-minus"></i></a>';
    $('.addMoreCertificate').click(function() {
        $('.clonnedItems').append($('.cloneThis').clone());
        $('.clonnedItems .cloneThis:last .col:last').html(deleteAppIcon);
        $('.clonnedItems .cloneThis').find('input').val('');
        $('.clonnedItems .cloneThis').find('select option').removeAttr('disabled').removeAttr('selected');
        $('.clonnedItems .cloneThis').find('input').val('');
        $('.clonnedItems .cloneThis').removeClass('cloneThis');
    });
    $('body').on('click', '.removeCertificate', function() {
        let index = $(this).index();
        $('.clonnedItems .row').eq(index).remove();
    });

    $('body').on('click', '.priceSelection', function() {
        $('.submitApplication').removeClass('disabled');
        $("input[name=service_price]").val($(this).val());
    });
});
</script>
@endpush