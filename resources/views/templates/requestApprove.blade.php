<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Request Approved</title>
  </head>
  <body class="">
    <table border="0" cellpadding="5" cellspacing="0" width="400">
        <tr>
            <th><h1>eServices</h1></th>
            <th align="right"><h6>Invoice</h6></th>
        </tr>
        <tr>
            <th></th>
            <th align="right">{{ date('d M, Y') }}</th>
        </tr>
        <tr style="border-bottom: 2px solid #000;">
            <td></td>
            <td align="right">Invoice {{ str_pad($data->id, 5, 0, STR_PAD_LEFT) }}</td>
        </tr>
        <tr>
            <td align="left" colspan="2"><b>Dear {{ ucfirst($data->full_name) }},</b></td>
        </tr>
        <tr>
            <td align="left">{{ $data->serviceInvoice->title }}</td>
            <td align="right">{{ $data->serviceInvoice->price }}</td>
        </tr>
        <tr>
            <td align="left">
                Vat
            </td>
            <td align="right">{{ $data->serviceInvoice->vat }}</td>
        </tr>
        <tr>
            <td align="right">Total</td>
            <td align="right">{{ $data->serviceInvoice->total }}</td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                Thank you - eServices
            </td>
        </tr>
    </table>
  </body>
</html>