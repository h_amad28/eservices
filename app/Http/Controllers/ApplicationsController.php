<?php

namespace App\Http\Controllers;

use App\Models\Applications;
use App\Models\ApplicationGraduations;
use App\Models\ApplicationImages;
use Illuminate\Http\Request;
use App\Mail\RequestEntertain;
use App\Mail\RequestApproved;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Gate;

class ApplicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('accept-user')) {
            return redirect('certify-user');
        }
        //
        $applications = Applications::where(['is_active' => 1])
        ->where('status', '!=', 'rejected')
        ->with(['applicationImages', 'applicationGraduations', 'serviceInvoice'])
        ->get();        
        return view('applications', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:App\Models\Applications,email',
            'phone' => 'required|numeric',
            'service_price' => 'required|numeric',
            'profilePicture' => 'required|mimes:jpg,png',
            'university' => 'required|array',
            'degree' => 'required|array',
            'certificate' => 'required|array',
            'certificate.*' => 'required|mimes:pdf',
        ]);
        try {
            $applicationObj = new Applications($request->all());
            $applicationObj->service_price_id = $request->get('service_price');
            if ($applicationObj->save()) {
                $path = $request->profilePicture->store(Applications::PROFILE_IMAGE_PATH);
                $profileImage = new ApplicationImages(['application_id' => $applicationObj->id, 'images' => $path]);
                $profileImage->save();
                foreach ($request->get('university') as $index => $value) {
                    $certificatePath = $request->certificate[$index]->store(ApplicationGraduations::GRADUATION_PATH);
                    $params = [
                        'application_id' => $applicationObj->id,
                        'university' => $value,
                        'education' => $request->get('degree')[$index],
                        'certificate' => $certificatePath,
                    ];
                    $appGraduationObj = new ApplicationGraduations($params);
                    $appGraduationObj->save();
                }
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        \Mail::to($request->get('email'))->queue(new RequestEntertain($applicationObj));
        return redirect('/certify-user')->with('form_response', 'Application submitted successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function show(Applications $applications)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function edit(Applications $applications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applications $applications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Applications  $applications
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applications $applications)
    {
        //
    }

    public function reject(Request $request) {
        if (! Gate::allows('accept-user')) {
            return response()->json([],400);
        }
        $id = $request->get('id');
        $app = Applications::find($id);
        $app->status = 'rejected';

        if ($app->save()) {
            return response()->json([],200);
        }
        return response()->json([],400);
    }

    public function accept(Request $request) {
        // if (! Gate::allows('accept-user')) {
        //     return response()->json([],400);
        // }
        $id = $request->get('id');
        $app = Applications::find($id);
        $app->status = 'initial acceptance';

        if ($app->save()) {
            return response()->json([],200);
        }
        return response()->json([],400);
    }

    public function approve(Request $request) {
        if (! Gate::allows('approve-user')) {
            return response()->json([],400);
        }
        $id = $request->get('id');
        $app = Applications::find($id);
        $app->status = 'approved';

        $path = storage_path('app') . '/applicant/invoice';
        if(!\File::exists($path)) {
            \File::makeDirectory($path, $mode = 0755, true, true);
        } 
        Pdf::loadView('templates.requestApprove', ['data' => $app])->save($path . '/' . $app->id . '-invoice.pdf');
        if ($app->save()) {
            \Mail::to($request->get('email'))->queue(new RequestApproved($app));
            return response()->json([],200);
        }
        return response()->json([],400);
    }
}
