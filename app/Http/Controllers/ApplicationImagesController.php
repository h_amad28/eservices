<?php

namespace App\Http\Controllers;

use App\Models\ApplicationImages;
use Illuminate\Http\Request;

class ApplicationImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ApplicationImages  $applicationImages
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationImages $applicationImages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ApplicationImages  $applicationImages
     * @return \Illuminate\Http\Response
     */
    public function edit(ApplicationImages $applicationImages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ApplicationImages  $applicationImages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApplicationImages $applicationImages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ApplicationImages  $applicationImages
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationImages $applicationImages)
    {
        //
    }
}
