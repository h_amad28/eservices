<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        return view('auth.login');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        session()->flush();
        return redirect()->route('login');
    }

    public function authenticate(Request $request)
    {
        try {
            $params = $request->input();
            $user = User::where('email', $request->email)->first();
            if (!empty($user)) {
                if (Auth::attempt(['email' => $params['email'], 'password' => $params['password']])) {
                    $view = "applications.index";
                    // if ($user->type == USER_TYPE_PM) {
                        // $view = 'app.index';
                    // }
                    return redirect('applications');//->route($view);
                }
            }
            return redirect('login')->with('error', "Invalid Login Details!")->onlyInput('email');
        } catch (\Exception $e) {
            // \Illuminate\Support\Facades\Log::info($e->getMessage());
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}