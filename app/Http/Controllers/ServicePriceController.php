<?php

namespace App\Http\Controllers;

use App\Models\ServicePrice;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class ServicePriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('prices')) {
            return redirect('/certify-user');
        }
        //
        $data = ServicePrice::where('is_active',1)->get();
        return view('service_price', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'price' => 'required|numeric',
            'vat' => 'required|numeric',
            'vat_type' => ['required', Rule::in(['fixed', 'percentage'])],
            'type' => ['required', Rule::in(['free', 'paid', 'certified'])],
        ]);
        $servicePrice = new ServicePrice($request->all());
        if ($servicePrice->save()) {
            return redirect('prices')->with('form_response', 'Saved successfully');
        }
        return redirect('prices');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServicePrice  $servicePrice
     * @return \Illuminate\Http\Response
     */
    public function show(ServicePrice $servicePrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServicePrice  $servicePrice
     * @return \Illuminate\Http\Response
     */
    public function edit(ServicePrice $servicePrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ServicePrice  $servicePrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServicePrice $servicePrice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServicePrice  $servicePrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServicePrice $servicePrice)
    {
        //
    }
}
