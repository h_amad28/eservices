<?php

namespace App\Http\Controllers;

use App\Models\ApplicationGraduations;
use Illuminate\Http\Request;

class ApplicationGraduationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ApplicationGraduations  $applicationGraduations
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationGraduations $applicationGraduations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ApplicationGraduations  $applicationGraduations
     * @return \Illuminate\Http\Response
     */
    public function edit(ApplicationGraduations $applicationGraduations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ApplicationGraduations  $applicationGraduations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApplicationGraduations $applicationGraduations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ApplicationGraduations  $applicationGraduations
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationGraduations $applicationGraduations)
    {
        //
    }
}
