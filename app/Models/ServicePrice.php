<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicePrice extends Model
{
    use HasFactory;
    protected $table = 'service_price';
    public $primaryKey = "id";
    protected $fillable = ['title', 'price', 'vat', 'vat_type', 'type'];

    function getTotalAttribute() {
        if ($this->getAttribute('vat_type') == 'fixed') {
            return ($this->price + $this->vat);
        } else {
            $vat = (($this->vat / $this->price) * 100);
            return round(($this->price + $vat), 2);
        }
    }
}
