<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    use HasFactory;
    protected $table = 'users';
    public $primaryKey = "id";
    protected $guarded = [];
    protected $fillable = ['email', 'report_file_details', 'is_active', 'tenant_id'];
    // public $timestamps = false;
}
