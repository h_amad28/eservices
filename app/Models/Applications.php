<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ApplicationImages;
use App\Models\ApplicationGraduations;
use App\Models\ServicePrice;

class Applications extends Model
{
    use HasFactory;

    const PROFILE_IMAGE_PATH = 'applicant/profile';
    protected $table = 'applications';
    public $primaryKey = "id";
    protected $guarded = [];
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'country'];

    function applicationImages(){
        return $this->hasOne(ApplicationImages::class, 'application_id', 'id');
    }

    function serviceInvoice(){
        return $this->hasOne(ServicePrice::class, 'id', 'service_price_id');
    }

    function applicationGraduations(){
        return $this->hasMany(ApplicationGraduations::class, 'application_id', 'id');
    }

    function getFullNameAttribute() : string {
        return $this->first_name . ' ' . $this->last_name;
    }
}
