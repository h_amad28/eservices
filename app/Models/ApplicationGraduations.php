<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationGraduations extends Model
{
    use HasFactory;
    const GRADUATION_PATH = 'applicant/graduation';
    protected $table = 'application_graduations';
    public $primaryKey = "id";
    protected $fillable = ['application_id', 'university', 'education', 'certificate'];
}
