<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationImages extends Model
{
    use HasFactory;
    protected $table = 'application_images';
    public $primaryKey = "id";
    protected $fillable = ['application_id', 'images'];
}
