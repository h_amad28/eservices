<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('prices', function (User $user) {
            return $user->type == 'admin' || $user->type == 'employee' || $user->type == 'manager';
        });

        Gate::define('accept-user', function (User $user) {
            return $user->type == 'admin' || $user->type == 'employee' || $user->type == 'manager';
        });

        Gate::define('approve-user', function (User $user) {
            return $user->type == 'admin' || $user->type == 'manager';
        });
    }
}
